# Utilisez une image de base appropriée pour votre application
FROM openjdk:11

# Copiez les fichiers nécessaires dans le conteneur
COPY target/*.jar app.jar

# Commande pour exécuter votre application lorsque le conteneur démarre
ENTRYPOINT ["java", "-jar", "app.jar"]

